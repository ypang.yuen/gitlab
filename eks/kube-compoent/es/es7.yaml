apiVersion: v1
kind: Namespace
metadata:
  name: elastic

---

kind: Service
apiVersion: v1
metadata:
  name: es7
  namespace: elastic
  labels:
    app: es7
spec:
  ports:
  - name: es-srv
    protocol: TCP
    port: 9200
  - name: es-dis
    protocol: TCP
    port: 9300
  selector:
    app: es7
  clusterIP: None

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: es7-cluster
  namespace: elastic
spec:
  serviceName: es7
  replicas: 3
  selector:
    matchLabels:
      app: es7
  template:
    metadata:
      labels:
        app: es7
    spec:
      containers:
      - name: es7
        image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
        imagePullPolicy: Always
        resources:
          limits:
            cpu: "1"
            memory: 3000Mi
          requests:
            cpu: 100m
            memory: 300Mi
        ports:
          - containerPort: 9200
            name: es-srv
            protocol: TCP
          - containerPort: 9300
            name: es-dis
            protocol: TCP
        securityContext:
          capabilities:
            add:
            - IPC_LOCK
            - SYS_RESOURCE
          privileged: true
        volumeMounts:
        - name: es7-data
          mountPath: /usr/share/elasticsearch/data
        env:
        - name: namespace
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        - name: node.name
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.name
        - name: cluster.name
          value: es7
        - name: NETWORK_HOST
          value: 0.0.0.0
        - name: discovery.seed_hosts
          value: >-
            es7-cluster-0.es7,es7-cluster-1.es7,es7-cluster-2.es7
        - name: cluster.initial_master_nodes
          value: >-
            es7-cluster-0,es7-cluster-1,es7-cluster-2
        - name: NODE_MASTER
          value: "true"
        - name: NODE_INGEST
          value: "true"
        - name: NODE_DATA
          value: "true"
        - name: ES_JAVA_OPTS
          value: -Xms2000m -Xmx2000m
        - name: ES_PLUGINS_INSTALL
          value: repository-s3
      initContainers:
      - name: fix-permissions
        image: busybox
        command: ["sh", "-c", "chown -R 1000:1000 /usr/share/elasticsearch/data"]
        securityContext:
          privileged: true
        volumeMounts:
        - name: es7-data
          mountPath: /usr/share/elasticsearch/data
      - name: increase-vm-max-map
        image: busybox
        command: ["sysctl", "-w", "vm.max_map_count=262144"]
        securityContext:
          privileged: true
      - name: increase-fd-ulimit
        image: busybox
        command: ["sh", "-c", "ulimit -n 65536"]
        securityContext:
          privileged: true
      restartPolicy: Always
      tolerations:
        - key: "node.kubernetes.io/pod-usage"
          operator: "Equal"
          value: "sys"
          effect: "NoSchedule"
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node.kubernetes.io/node-affinity
                operator: In
                values:
                - sys
              - key: node.kubernetes.io/pod-anti-affinity
                operator: DoesNotExist
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - es7
            topologyKey: kubernetes.io/hostname
      schedulerName: default-scheduler
      securityContext: {}
  volumeClaimTemplates:
  - metadata:
      name: es7-data
      labels:
        app: es7
    spec:
      accessModes:
      - ReadWriteOnce
      storageClassName: gp2
      resources:
        requests:
          storage: 10Gi
      volumeMode: Filesystem