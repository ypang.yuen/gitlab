#!/bin/bash
set -e

echo "Enter AWS Profile to restore gitlab...:"

read PROFILE

ACCOUNT_ID=$(aws sts get-caller-identity --profile $PROFILE | jq -r ".Account")

if [ $ACCOUNT_ID = "" ]; then
  echo "Account not exist for profile - $PROFILE"
  exit 1;
fi


REGION="ap-southeast-1"
CLUSTER="fwd-moments-git-eks"
K8S_PROFILE="arn:aws:eks:$REGION:$ACCOUNT_ID:cluster/$CLUSTER"
REGISTRY="${ACCOUNT_ID}.dkr.ecr.${REGION}.amazonaws.com"

echo "K8S Profile ARN is:";
echo $K8S_PROFILE;

runk8s() {
  kubectl --context="$K8S_PROFILE" $@
}

runHelm() {
  helm --kube-context="$K8S_PROFILE" $@
}

# kubectl.git -n gitlab get secret gitlab-rails-secret -o jsonpath="{.data.secrets\.yml}" | base64 -d > old.yml

# # kubectl.git -n gitlab create secret generic gitlab-rails-secret --from-file=secrets.yml=secrets.yml

kubectl.git -n gitlab delete pods -lapp=sidekiq,release=gitlab
kubectl.git -n gitlab delete pods -lapp=webservice,release=gitlab
kubectl.git -n gitlab delete pods -lapp=task-runner,release=gitlab
# PODS=$(kubectl.git -n gitlab get pods -lrelease=gitlab,app=task-runner | grep gitlab | cut -d ' ' -f 1);
# while [[ $($$PODS -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for pod" && sleep 1; done


# kubectl.git exec -n gitlab $PODS -it -- backup-utility --restore -t 1635490978_2021_10_29_14.2.3-ee

# kubectl.git -n gitlab delete pods -lrelease=gitlab

# kubectl.git -n gitla get pods -lrelease=gitlab,app=task-runner

# kubectl.git exec -n gitlab $PODS -it -- gitlab-rails runner -e production /scripts/custom-instance-setup

# kubectl.git -n gitlab delete pods -lrelease=gitlab
kubectl.git -n gitlab get pods -lrelease=gitlab,app=webservice

PODS=$(runk8s -n gitlab get pods -lrelease=gitlab,app=webservice | grep gitlab | cut -d ' ' -f 1);
echo $PODS


while [[ $(runk8s -n gitlab get pods -lrelease=gitlab,app=webservice -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for pod" && sleep 1; done