#!/bin/bash
set -e

echo "Enter AWS Profile to install gitlab...:"

read PROFILE

ACCOUNT_ID=$(aws sts get-caller-identity --profile $PROFILE | jq -r ".Account")

if [ $ACCOUNT_ID = "" ]; then
  echo "Account not exist for profile - $PROFILE"
  exit 1;
fi

echo "K8S Profile ARN is:";
echo $K8S_PROFILE;

REGION="ap-southeast-1"
CLUSTER="fwd-moments-git-eks"
K8S_PROFILE="arn:aws:eks:$REGION:$ACCOUNT_ID:cluster/$CLUSTER"
REGISTRY="${ACCOUNT_ID}.dkr.ecr.${REGION}.amazonaws.com"

runk8s() {
  kubectl --context="$K8S_PROFILE" $@
}

runHelm() {
  helm --kube-context="$K8S_PROFILE" $@
}

echo "Pushing busybox to ECR";

aws ecr get-login-password --region ap-southeast-1 --profile itpunch | docker login --username AWS --password-stdin 154723115757.dkr.ecr.ap-southeast-1.amazonaws.com/busybox

docker pull 154723115757.dkr.ecr.ap-southeast-1.amazonaws.com/busybox:latest

aws ecr get-login-password --region ap-southeast-1 --profile $PROFILE | docker login --username AWS --password-stdin $REGISTRY

docker tag 154723115757.dkr.ecr.ap-southeast-1.amazonaws.com/busybox:latest $REGISTRY/busybox:custom

docker push $REGISTRY/busybox:custom


echo "Add Namespace for gitlab if not exist...";
runk8s apply -f ./namespace.yaml

echo "Add Secret for object storage connection...";
runk8s create secret generic gitlab-storage --from-file=connection=./secret/general.s3.yml -n gitlab
runk8s create secret generic storage-config --from-file=config=./secret/storage.config -n gitlab
runk8s create secret generic gitlab-postgresql-password --from-file=postgresql-password=./secret/psql_pwd.config  -n gitlab
runk8s create secret generic gitlab-runner-secret  -n gitlab \
    --from-literal=accesskey="AKIAY23DYKO2MSG2VH73" \
    --from-literal=secretkey="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"
