#!/bin/bash
set -e

echo "Enter AWS Profile to install CA, Calico...:"

read PROFILE

ACCOUNT_ID=$(aws sts get-caller-identity --profile $PROFILE | jq -r ".Account")

if [ $ACCOUNT_ID = "" ]; then
  echo "Account not exist for profile - $PROFILE"
  exit 1;
fi

REGION="ap-southeast-1"
AUTOSCALER_ROLE="fwd-moments-git-eks-cluster-autoscaler"

CLUSTER="fwd-moments-git-eks"

K8S_PROFILE="arn:aws:eks:$REGION:$ACCOUNT_ID:cluster/$CLUSTER"

echo "K8S Profile ARN is:";
echo $K8S_PROFILE;

runk8s() {
  kubectl --context="$K8S_PROFILE" $@
}

runHelm() {
  helm --kube-context="$K8S_PROFILE" $@
}

echo "Installing Calico...";
PODS=$(runk8s -n kube-system get pods -l k8s-app=calico-kube-controllers 2>&1);
if [[ $PODS =~ "No resources found"  ]]; then
  runk8s delete daemonset -n kube-system aws-node --ignore-not-found=true
  runk8s apply -n kube-system -f https://docs.projectcalico.org/archive/v3.17/manifests/calico-vxlan.yaml
  echo "Calico installed";
else
  echo "Calico exist, skip install";
fi


echo "Generating CA Deployment File...";
sed "s/{{ACCOUNT_ID}}/$ACCOUNT_ID/g; s:{{ROLE}}:$AUTOSCALER_ROLE:g; s/{{CLUSTER}}/$CLUSTER/g;" "./cluster-autoscaler/deployment.yaml" > "./cluster-autoscaler-deployment.yaml";

if [ ! -f "./cluster-autoscaler-deployment.yaml" ]; then
  echo "Generating CA Deployment File fail";
  exit 1;
fi

echo "Apply cluster-autoscaler";
runk8s apply -n kube-system -f ./cluster-autoscaler-deployment.yaml


echo "Apply aws-node-termination-handler";
PODS=$(runk8s -n kube-system get pods -l k8s-app=calico-kube-controllers 2>&1);

runHelm install aws-node-termination-handler eks/aws-node-termination-handler \
  --namespace kube-system \
  --values ./node-termination-handler/values.yaml

# sed " "

# runk8s apply -n kube-system ./cluster-autoscaler/deployment.yaml


# runk8s apply -n kube-system ./cluster-autoscaler/deployment.yaml

# runHelm install aws-node-termination-handler eks/aws-node-termination-handler \
#   --namespace kube-system \
#   --values ./node-termination-handler/values.yaml