#!/bin/bash
set -e

echo "Enter AWS Profile to install sonarqube...:"

read PROFILE

ACCOUNT_ID=$(aws sts get-caller-identity --profile $PROFILE | jq -r ".Account")

if [ $ACCOUNT_ID = "" ]; then
  echo "Account not exist for profile - $PROFILE"
  exit 1;
fi

echo "K8S Profile ARN is:";
echo $K8S_PROFILE;

REGION="ap-southeast-1"
CLUSTER="fwd-moments-git-eks"
K8S_PROFILE="arn:aws:eks:$REGION:$ACCOUNT_ID:cluster/$CLUSTER"
REGISTRY="${ACCOUNT_ID}.dkr.ecr.${REGION}.amazonaws.com"


runk8s() {
  kubectl --context="$K8S_PROFILE" $@
}

runHelm() {
  helm --kube-context="$K8S_PROFILE" $@
}

echo "Add Namespace for sonarqube if not exist...";
runk8s apply -f ./namespace.yaml

REGISTRY="${ACCOUNT_ID}.dkr.ecr.${REGION}.amazonaws.com"

echo "Pushing sonarqube to ECR";

aws ecr get-login-password --region ap-southeast-1 --profile $PROFILE | docker login --username AWS --password-stdin $REGISTRY

docker pull nosinovacao/dotnet-sonar:21.07.1

docker tag nosinovacao/dotnet-sonar:21.07.1 $REGISTRY/sonarqube_dotnet:21.07.1

docker push $REGISTRY/sonarqube_dotnet:21.07.1
