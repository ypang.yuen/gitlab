module "ca" {
  source = "./cluster-autoscaler"
  depends_on = [
    module.eks
  ]

  prefix               = local.cluster_name
  oidc_issuer_url      = module.eks.cluster_oidc_issuer_url
  oidc_provider_arn    = module.eks.oidc_provider_arn
  service_account_name = var.k8s_ca_service_account_name
  namespace            = "kube-system"

  # provisioner "local-exec" {
  #   command = "echo ${self.arn} >> arn.txt"
  # }
}
