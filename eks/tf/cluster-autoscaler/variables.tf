variable "prefix" {
  description = "Prefix"
  type        = string
}

variable "oidc_issuer_url" {
  type        = string
}

variable "oidc_provider_arn" {
  type        = string
}

variable "namespace" {
  type        = string
}

variable "service_account_name" {
  type        = string
}