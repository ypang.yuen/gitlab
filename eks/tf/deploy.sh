#!/bin/bash
set -e

ENVIRONMENT="infra"

echo $ENVIRONMENT

echo "Enter Profile: "

read PROFILE

echo "Add Node? "
read ADD_NODE


terraform validate

terraform plan -var-file="./tfvar/${ENVIRONMENT}.tfvars" \
 -var="profile=$PROFILE" \
 -var="k8s_add_node=$(if [[ "${ADD_NODE}" == true ]]; then echo true; else echo false; fi)" \
 -out="plan/${ENVIRONMENT}.plan"

echo "Confirm to process, (Y)es or (N)o?"
read CONFIRM

if [[ $CONFIRM == "Y" ]] || [[ $CONFIRM == "y" ]]
then
  terraform apply "plan/${ENVIRONMENT}.plan"
  rm -rf "plan/${ENVIRONMENT}.plan"
  exit 0;
else
  echo "Aborted.";
  rm -rf "plan/${ENVIRONMENT}.plan"
  clear;
  exit 0;
fi
