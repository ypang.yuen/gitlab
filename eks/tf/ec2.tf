resource "aws_security_group" "gitlab_ec2_bastion_sg" {
  name   = "${local.cluster_name}-bastion-sg"
  vpc_id = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "gitlab_ec2_bastion" {
  source                      = "terraform-aws-modules/ec2-instance/aws"
  version                     = "~> 2.0"
  name                        = "${local.cluster_name}-bastion"
  instance_count              = 1
  ami                         = "ami-0cd31be676780afa7"
  instance_type               = "t2.micro"
  key_name                    = var.eks_worker_node_key_pair
  monitoring                  = false
  vpc_security_group_ids      = [aws_security_group.gitlab_ec2_bastion_sg.id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  tags = {
    Usage = "SSH bastion"
  }
}
