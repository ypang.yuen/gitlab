##deleted
provider "kubernetes" {
  config_path = module.eks.kubeconfig_filename
  token       = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = local.cluster_version
  subnets         = module.vpc.private_subnets

  tags = {
    Environment                                       = var.environment
    "k8s.io/cluster-autoscaler/${local.cluster_name}" = "owned"
    "k8s.io/cluster-autoscaler/enabled"               = true
  }
  vpc_id                                = module.vpc.vpc_id
  cluster_enabled_log_types             = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  cluster_endpoint_public_access        = true
  cluster_endpoint_public_access_cidrs  = ["0.0.0.0/0"]
  cluster_endpoint_private_access       = true
  cluster_endpoint_private_access_cidrs = ["0.0.0.0/0"]
  enable_irsa                           = true
  worker_groups_launch_template         = local.eks_launch_template
}


data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}



# resource "aws_autoscaling_schedule" "eks_asg_schedule_down" {
#     for_each = {
#         for k, asg in module.eks.workers_asg_names : "asg.${k}.down" => asg
#     }

#     scheduled_action_name = "${each.value}-down"
#     min_size               = 0
#     max_size               = length(regexall("sys-nodes", each.value)) > 0 ?  var.eks_autoscaling_sys_max_size : var.eks_autoscaling_app_max_size
#     desired_capacity       = 0
#     recurrence             = "* 17 * * 1-7"
#     autoscaling_group_name = each.value
# }

# resource "aws_autoscaling_schedule" "eks_asg_schedule_up" {
#     for_each = {
#         for k, asg in module.eks.workers_asg_names : "asg.${k}.up" => asg
#     }

#     scheduled_action_name = "${each.value}-up"
#     min_size               = 0
#     max_size               = length(regexall("sys-nodes", each.value)) > 0 ?  var.eks_autoscaling_sys_max_size : var.eks_autoscaling_app_max_size
#     desired_capacity       = 1
#     recurrence             = "* 14 * * 1-7"
#     autoscaling_group_name = each.value
# }
