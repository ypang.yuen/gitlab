locals {
  vpc_ips = [for i, v in module.vpc.nat_public_ips : "${v}/32"]
}

module "gitlab" {
  source = "./gitlab"
  depends_on = [
    module.eks
  ]
  subnet_cidrs      = var.private_subnets
  subnet_ids        = module.vpc.private_subnets
  vpc_id            = module.vpc.vpc_id
  prefix            = "fwdmoments"
  role_prefix       = local.cluster_name
  s3_prefix         = local.cluster_name
  oidc_issuer_url   = module.eks.cluster_oidc_issuer_url
  oidc_provider_arn = module.eks.oidc_provider_arn
  rds_instance_type = var.gitlab_rds_instance_type
  cluster_sg_id     = module.eks.cluster_security_group_id
  ip_whitelist      = var.ip_whitelist
  ip_whitelist2     = var.ip_whitelist2
  https_ip_whitelist = var.https_ip_whitelist
  ip_whitelist_for_system = concat(var.ip_whitelist_for_system, local.vpc_ips)
}

# data "aws_elb" "gitlab_lb" {
#   # tags = {
#   #   "kubernetes.io/service-name"   = "gitlab/gitlab-nginx-ingress-controller"
#   # }
#   name = "a5932682767bb4e9b86b5e2f86cd6eb5"
# }


