resource "aws_ecr_repository" "gitlab_busybox_ecr" {
  name                 = "busybox"
}

resource "aws_ecr_repository" "sonarqube_dotnet_ecr" {
  name                 = "sonarqube_dotnet"
}