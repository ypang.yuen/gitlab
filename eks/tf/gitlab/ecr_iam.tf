data "aws_iam_policy" "gitlab_runner" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

data "aws_iam_policy_document" "gitlab_runner_assume" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(var.oidc_issuer_url, "https://", "")}:sub"
      values = [
        "system:serviceaccount:${var.namespace}:${var.runner_service_account_name}",
      ]
    }

    principals {
      identifiers = [var.oidc_provider_arn]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "gitlab_runner" {
  name               = "${var.role_prefix}-gitlab-runner"
  assume_role_policy = data.aws_iam_policy_document.gitlab_runner_assume.json
}

resource "aws_iam_role_policy_attachment" "gitlab_runner_policy_attachment" {
  role       = aws_iam_role.gitlab_runner.name
  policy_arn = data.aws_iam_policy.gitlab_runner.arn
}


resource "aws_iam_policy" "gitlab_sam_policy" {
  name        = "sam-policy"
  description = "Sam policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "*",
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "gitlab_runner_sam_policy_attachment" {
  role       = aws_iam_role.gitlab_runner.name
  policy_arn = aws_iam_policy.gitlab_sam_policy.arn
}

