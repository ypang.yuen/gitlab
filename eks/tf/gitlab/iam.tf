locals {
  s3_policy_resources_bucket_name    = [for i, v in var.s3_buckets_names : "arn:aws:s3:::${var.s3_prefix}-${v}"]
  s3_policy_resources_bucket_content = [for i, v in var.s3_buckets_names : "arn:aws:s3:::${var.s3_prefix}-${v}/*"]
}

data "aws_iam_policy_document" "gitlab_s3_policy" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]

  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
      "s3:ListBucketMultipartUploads"
    ]
    resources = local.s3_policy_resources_bucket_name
    effect    = "Allow"
  }

  statement {
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:DeleteObject",
      "s3:ListMultipartUploadParts",
      "s3:AbortMultipartUpload"
    ]
    resources = local.s3_policy_resources_bucket_content
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "gitlab_s3" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]
  name   = "${var.s3_prefix}-s3-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.gitlab_s3_policy.json
}

data "aws_iam_policy_document" "gitlab_s3_assume" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(var.oidc_issuer_url, "https://", "")}:sub"
      values = [
        "system:serviceaccount:${var.namespace}:${var.service_account_name}",
      ]
    }

    principals {
      identifiers = [var.oidc_provider_arn]
      type        = "Federated"
    }
  }
}


resource "aws_iam_role" "gitlab_s3_assume_role" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]
  name               = "${var.s3_prefix}-gitlab-s3-assume-role"
  assume_role_policy = data.aws_iam_policy_document.gitlab_s3_assume.json
}

resource "aws_iam_role_policy_attachment" "gitlab_s3_policy_attachment" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]
  role       = aws_iam_role.gitlab_s3_assume_role.name
  policy_arn = aws_iam_policy.gitlab_s3.arn
}

resource "aws_iam_user" "gitlab_s3_user" {
  name = "gitlab_s3_user"
  tags = {
    name : "gitlab_s3_user"
  }
}

resource "aws_iam_user_policy_attachment" "gitlab_s3_user_attachment" {
  user       = aws_iam_user.gitlab_s3_user.name
  policy_arn = aws_iam_policy.gitlab_s3.arn
}



# module "gitlab_s3_user" {
#   source                        = "terraform-aws-modules/iam/aws//modules/iam-user"
#   version                       = "4.7.0"
#   name                          = "gitlab_s3_user"
#   path                          = "/"
#   create_iam_user_login_profile = false
#   force_destroy                 = true
#   pgp_key                       = "keybase:stanleyyuen"
#   permissions_boundary          = "" // default
#   tags = {
#     name : "gitlab_s3_user"
#   }
# }
