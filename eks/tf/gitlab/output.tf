output "arn" {
  value = aws_iam_role.gitlab_s3_assume_role.arn
}

output "rds_pwd" {
  value = "p${random_password.db_password.result}"
  sensitive   = true
}

