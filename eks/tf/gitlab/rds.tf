resource "random_password" "db_password" {
  length  = 12
  special = false
  upper   = true
}
resource "random_string" "db_suffix" {
  length  = 4
  special = false
  upper   = false
}

resource "aws_db_instance" "gitlab_core" {

  # Engine options
  engine         = "postgres"
  engine_version = "12.7"

  # Settings
  name       = "gitlab_postgresql"
  identifier = "gitlab-core-rds"

  # Credentials Settings
  username = "gitlab"
  password = "p${random_password.db_password.result}"

  # DB instance size
  instance_class = var.rds_instance_type

  # Storage
  storage_type          = "gp2"
  allocated_storage     = var.rds_storage
  max_allocated_storage = var.rds_max_storage

  # Availability & durability
  multi_az = var.rds_multi_az

  # Connectivity
  db_subnet_group_name = aws_db_subnet_group.sg.id

  publicly_accessible    = false
  vpc_security_group_ids = [aws_security_group.sg.id]
  port                   = var.rds_port

  # Database authentication
  iam_database_authentication_enabled = false

  # Additional configuration
  parameter_group_name = "default.postgres12"

  # Backup
  # backup_retention_period             = 14
  # backup_window                       = "03:00-04:00"
  # final_snapshot_identifier           = "gitlab-postgresql-final-snapshot-${random_string.db_suffix.result}" 
  # delete_automated_backups            = true
  # skip_final_snapshot                 = false

  # Encryption
  storage_encrypted = var.rds_encrypt

  # Maintenance
  auto_minor_version_upgrade = true
  maintenance_window         = "Sat:00:00-Sat:02:00"

  # Deletion protection
  deletion_protection = false

  tags = {
    Environment = "core"
    Name        = "${var.prefix}-gitlab-postgresql"
  }
}
