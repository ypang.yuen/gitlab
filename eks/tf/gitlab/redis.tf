# resource "aws_elasticache_cluster" "gitlab" {
#   cluster_id           = "cluster-gitlab"
#   engine               = "redis"
#   node_type            = "cache.m4.xlarge"
#   num_cache_nodes      = 1
#   parameter_group_name = "default.redis3.2"
#   engine_version       = "3.2.10"
#   port                 = 6379
#   subnet_group_name    = aws_elasticache_subnet_group.gitlab.name
#   security_group_ids   = [aws_security_group.redis.id]
# }

# resource "aws_elasticache_subnet_group" "gitlab" {
#   name       = "gitlab-cache-subnet"
#   subnet_ids = var.subnet_ids
# }

# resource "aws_security_group" "redis" {
#   name   = "gitlab-cache-sg"
#   vpc_id = var.vpc_id

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "gitlab-cache-sg"
#   }
# }

# resource "aws_security_group_rule" "redis_inbound" {
#   from_port                = 6379
#   protocol                 = "tcp"
#   security_group_id        = aws_security_group.redis.id
#   source_security_group_id = var.cluster_sg_id
#   to_port                  = 6379
#   type                     = "ingress"
# }
