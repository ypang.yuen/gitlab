resource "aws_s3_bucket" "gitlab_buckets" {
  for_each = toset(var.s3_buckets_names)
  bucket = "${var.s3_prefix}-${each.value}"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Name        = "${var.s3_prefix}-${each.value}"
    Usage       = "Gitlab"
  }
}

resource "aws_s3_bucket_public_access_block" "gitlab_buckets_public_access_block" {
  depends_on = [
    aws_s3_bucket.gitlab_buckets
  ]
  for_each = {
    for k,v in aws_s3_bucket.gitlab_buckets : "gitlab_buckets.${k}" => v.id
  }
  bucket = each.value
  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}