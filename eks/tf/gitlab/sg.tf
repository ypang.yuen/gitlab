resource "aws_db_subnet_group" "sg" {
  name       = "${var.prefix}-git-db-sg"
  subnet_ids =  var.subnet_ids

  tags = {
    Name        = "${var.prefix}-git-db-sg"
  }
}

resource "aws_security_group" "sg" {
  name        = "${var.prefix}-git-db-sg"
  description = "Allow inbound/outbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = var.rds_port
    to_port         = var.rds_port
    protocol        = "tcp"
    cidr_blocks =  var.subnet_cidrs
  }

  egress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks =  var.subnet_cidrs
  }

  tags = {
    Name        = "${var.prefix}-git-db-sg"
  }
}

resource "aws_security_group" "staff_sg" {
  name = "${var.prefix}-git-sg"
  description = "Allow inbound/outbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = var.ip_whitelist
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.prefix}-git-sg"
  }
}

resource "aws_security_group" "staff_sg_2" {
  name = "${var.prefix}-git-sg-2"
  description = "Allow inbound/outbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = var.ip_whitelist2
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.prefix}-git-sg2"
  }
}

resource "aws_security_group" "system_sg" {
  name = "${var.prefix}-git-sg-system"
  description = "Allow inbound/outbound traffic"
  vpc_id      = var.vpc_id
  
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = var.ip_whitelist_for_system
  }

  tags = {
    Name        = "${var.prefix}-git-sg-system"
  }
}

