variable "s3_prefix" {
  description = "S3 Prefix"
  type        = string
}

variable "prefix" {
  description = "Prefix"
  type        = string
}

variable "s3_buckets_names" {
  description = "S3 Bucket Names"
  type        = list(string)
  default = ["gitlab-registry", "gitlab-lfs", "gitlab-packages", "gitlab-pseudo",
  "gitlab-uploads", "gitlab-artifacts", "gitlab-pages", "gitlab-backup", "gitlab-tmp", "gitlab-runner-cache"]
}

variable "oidc_issuer_url" {
  type = string
}

variable "oidc_provider_arn" {
  type = string
}

variable "namespace" {
  type = string
  default = "gitlab"
}

variable "service_account_name" {
  type    = string
  default = "gitlab-service-account"
}

variable "runner_service_account_name" {
  type    = string
  default = "gitlab-gitlab-runner"
}

variable "subnet_ids" {
  type = list(string)
}

variable "subnet_cidrs" {
  type = list(string)
}

variable "vpc_id" {
  type = string
}

variable "rds_port" {
  type    = number
  default = 5432
}

variable "rds_instance_type" {
  type    = string
  default = "db.t3.medium"
}

variable "rds_storage" {
  type    = number
  default = 100
}

variable "rds_max_storage" {
  type    = number
  default = 1000
}

variable "rds_encrypt" {
  type    = bool
  default = false
}

variable "rds_multi_az" {
  type    = bool
  default = false
}

variable "cluster_sg_id" {
  type = string
}

variable "role_prefix" {
  type = string
}

variable "ip_whitelist" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}

variable "https_ip_whitelist" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}

variable "ip_whitelist2" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}

variable "ip_whitelist_for_system" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}

