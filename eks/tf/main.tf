locals {
  cluster_name           = "fwd-moments-${var.suffix}-eks"
  cluster_version        = "1.19"
  vpc_name               = "fwd-moments-${var.suffix}-vpc"
  worker_ami_owner_id    = "602401143452"
  worker_ami_name_filter = "amazon-eks-node-${local.cluster_version}-v*"
}

