
output "eks_issuer_url" {
    value = module.eks.cluster_oidc_issuer_url
}

output "eks_openid_arn" {
    value = module.eks.oidc_provider_arn
}

output "ca_arn" {
    value = module.ca.arn
}

output "gitlab_arn" {
    value = module.gitlab.arn
}

output "gitlab_rds_pwd" {
    value = module.gitlab.rds_pwd
    sensitive   = true
}

output "nat_public_ips" {
    value = module.vpc.nat_public_ips
}


# output "alb" {
#   value = data.aws_elb.gitlab_lb.name
# }