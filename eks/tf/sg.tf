resource "aws_security_group" "eks_worker_node" {
  name_prefix = "eks_worker_node_sg"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [
      var.cidr_block,
    ]
  }

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"

    cidr_blocks = [
      var.cidr_block,
    ]
  }
  tags = {
    Name        = "eks_worker_node_sg"
    Environment = var.environment
    EksCluster  = local.cluster_name
  }
}
