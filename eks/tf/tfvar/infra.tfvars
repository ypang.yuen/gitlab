aws_region                        = "ap-southeast-1"
suffix                            = "git"
environment                       = "dev"
cidr_block                        = "16.0.0.0/16"
private_subnets                   = ["16.0.0.0/24", "16.0.1.0/24", "16.0.2.0/24"]
public_subnets                    = ["16.0.10.0/24", "16.0.11.0/24", "16.0.12.0/24"]
eks_autoscaling_volume_size       = "40"
eks_autoscaling_app_max_size      = "10"
eks_autoscaling_runner_max_size   = "3"
eks_autoscaling_app_instance_type = ["t3.large", "t3.xlarge"]
eks_autoscaling_sys_instance_type = ["t3.medium", "t3.large"]
eks_autoscaling_spot_max_price    = "0.1"
k8s_namespace                     = "kube-system"
k8s_ca_service_account_name       = "cluster-autoscaler"
eks_worker_node_key_pair          = "moments-git-worker-node"
gitlab_rds_instance_type          = "db.t3.small"

ip_whitelist = [

]

ip_whitelist2=[
 
]

ip_whitelist_for_system = [
 
]
https_ip_whitelist = [

]

