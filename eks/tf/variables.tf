variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "ap-southeast-1"
}

variable "profile" {
  description = "AWS region"
  type        = string
}

variable "cidr_block" {
  type    = string
  default = "16.0.0.0/16"
}

variable "eks_worker_node_key_pair" {
  type = string
}

variable "private_subnets" {
  type    = list(string)
  default = ["16.0.0.0/24", "16.0.1.0/24", "16.0.2.0/24"]
}

variable "public_subnets" {
  type    = list(string)
  default = ["16.0.10.0/24", "16.0.11.0/24", "16.0.12.0/24"]
}

variable "suffix" {
  type    = string
  default = "git"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "eks_autoscaling_volume_size" {
  type    = string
  default = "10"
}

variable "eks_autoscaling_app_max_size" {
  type    = string
  default = "10"
}

variable "eks_autoscaling_runner_max_size" {
  type    = string
  default = "5"
}

variable "eks_autoscaling_app_instance_type" {
  type    = list(string)
  default = ["m3.medium", "m4.large"]
}

variable "eks_autoscaling_runner_instance_type" {
  type    = list(string)
  default = ["t3.medium", "t3.large"]
}

variable "eks_autoscaling_spot_max_price" {
  type    = string
  default = "0.1"
}

variable "k8s_namespace" {
  type    = string
  default = "kube-system"
}

variable "k8s_ca_service_account_name" {
  type    = string
  default = "cluster-autoscaler"
}

variable "k8s_gitlab_service_account_name" {
  type    = string
  default = "gitlab-service-account"
}

variable "k8s_gitlab_runner_service_account_name" {
  type    = string
  default = "gitlab-runner-service-account"
}

variable "k8s_add_node" {
  type    = bool
  default = false
}

variable "gitlab_rds_instance_type" {
  type    = string
  default = "db.m5.large"
}

variable "ip_whitelist" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}
variable "ip_whitelist2" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}


variable "https_ip_whitelist" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}

variable "ip_whitelist_for_system" {
  type        = list(string)
  description = "IP Whitelist for Internet Access to Git"
}
