locals {
  worker_groups_launch_template_app = [
    {
      name                     = "app-nodes-group-a"
      spot_allocation_strategy = "lowest-price"
      //spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_app_instance_type
      cpu_credits                              = "standard"
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_app_instance_type)
      on_demand_base_capacity                  = "1"
      on_demand_percentage_above_base_capacity = "100"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_app_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[0]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    },
    {
      name                     = "app-nodes-group-b"
      spot_allocation_strategy = "lowest-price"
      // spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_app_instance_type
      cpu_credits                              = "standard"
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_app_instance_type)
      on_demand_base_capacity                  = "1"
      on_demand_percentage_above_base_capacity = "100"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_app_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[1]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    }
    ,
    {
      name                     = "app-nodes-group-c"
      spot_allocation_strategy = "lowest-price"
      // spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_app_instance_type
      cpu_credits                              = "standard"
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_app_instance_type)
      on_demand_base_capacity                  = "1"
      on_demand_percentage_above_base_capacity = "100"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_app_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[2]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    }
  ]
  worker_groups_launch_template_runner = [
    {
      name                     = "runner-nodes-group-a"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=runner  --register-with-taints=node.kubernetes.io/pod-usage=runner:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[0]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    },
    {
      name                     = "runner-nodes-group-b"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=runner  --register-with-taints=node.kubernetes.io/pod-usage=runner:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[1]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    },
    {
      name                     = "runner-nodes-group-c"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=runner  --register-with-taints=node.kubernetes.io/pod-usage=runner:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[2]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    }
  ]

  worker_groups_launch_template_sonarqube = [
    {
      name                     = "sonarqube-nodes-group-a"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=sonarqube  --register-with-taints=node.kubernetes.io/pod-usage=sonarqube:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[0]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    },
    {
      name                     = "sonarqube-nodes-group-b"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=sonarqube  --register-with-taints=node.kubernetes.io/pod-usage=sonarqube:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[1]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    },
    {
      name                     = "sonarqube-nodes-group-c"
      spot_allocation_strategy = "lowest-price"
      //  spot_max_price                              = var.eks_autoscaling_spot_max_price
      override_instance_types                  = var.eks_autoscaling_runner_instance_type
      enable_monitoring                        = false
      spot_instance_pools                      = length(var.eks_autoscaling_runner_instance_type)
      on_demand_base_capacity                  = "0"
      on_demand_percentage_above_base_capacity = "0"
      ami_id_linux                             = data.aws_ami.eks_worker.id
      key_name                                 = var.eks_worker_node_key_pair
      bootstrap_extra_args                     = "--use-max-pods false"
      kubelet_extra_args                       = "--max-pods=100 --node-labels=node.kubernetes.io/lifecycle=spot,node.kubernetes.io/node-affinity=sonarqube  --register-with-taints=node.kubernetes.io/pod-usage=sonarqube:NoSchedule"
      additional_userdata                      = "using spot price"
      asg_desired_capacity                     = "1"
      asg_min_size                             = "0"
      asg_max_size                             = var.eks_autoscaling_runner_max_size
      root_volume_size                         = var.eks_autoscaling_volume_size
      subnets                                  = [module.vpc.private_subnets[2]]
      additional_security_group_ids            = [aws_security_group.eks_worker_node.id]
    }
  ]
  eks_launch_template = concat(local.worker_groups_launch_template_app,  local.worker_groups_launch_template_sonarqube)
}

data "aws_ami" "eks_worker" {
  filter {
    name   = "name"
    values = [local.worker_ami_name_filter]
  }

  most_recent = true

  owners = [local.worker_ami_owner_id]
}
